import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Geolocation from '@react-native-community/geolocation';
import { fecthClimaForecast } from '../store/forecastDay/actions';

export function useFetchData() {
  const [loading, setLoading] = useState(false);
  const network = useSelector(state => state.network);
  const dispatch = useDispatch();

  useEffect(() => {
    setLoading(true);
    Geolocation.getCurrentPosition(position => {
      dispatch(
        fecthClimaForecast(
          position.coords.latitude,
          position.coords.longitude,
          network.isConnected,
        ),
      );
    });
    const timeOut = setTimeout(() => setLoading(false), 2000);
    timeOut;
    return () => clearTimeout(timeOut);
  }, [network]);

  return loading;
}
