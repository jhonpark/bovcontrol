import React from 'react';
import { useSelector } from 'react-redux';

import { Container, ListWeather } from './styles';
import Weather from '../../boards/Weather';

function Details() {
  const listForecasDay = useSelector(state => state.clima.forecast);

  return (
    <Container>
      <ListWeather
        data={listForecasDay.forecast?.forecastday}
        keyExtrac
        renderItem={({ item }) => <Weather weather={item} />}
      />
    </Container>
  );
}

export default Details;
