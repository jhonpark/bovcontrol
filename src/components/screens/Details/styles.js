import styled from 'styled-components/native';

export const Container = styled.SafeAreaView`
  flex: 1;
  background-color: #fff;
`;

export const ListWeather = styled.FlatList.attrs({
  showsVerticalScrollIndicator: false,
})``;

export const LoadView = styled.View`
  flex: 1;
  align-items: center;
  justify-content: center;
`;
