import styled from 'styled-components/native';
import { colors } from '../../../utils/colors';

export const Container = styled.View`
  flex: 1;
  padding: 5px 10px;
  background: ${colors.white};
`;

export const TitleLocale = styled.Text`
  margin: 10px;
  font-size: 30px;
  font-weight: bold;
  text-align: center;
`;

export const Content = styled.View`
  justify-content: center;
  flex-direction: row;
  margin-top: 30px;
`;

export const LeftCondition = styled.View`
  width: 50%;
  align-items: center;
`;

export const Icon = styled.Image`
  width: 120px;
  height: 110px;
`;

export const LabelCondition = styled.Text`
  font-size: 17px;
  font-weight: bold;
  text-align: center;
`;

export const RightTemp = styled.View`
  width: 50%
  align-items: center;
  justify-content: center;
  
`;

export const LabelTemp = styled.Text`
  text-align: center;
  font-size: 80px;
  font-weight: bold;
`;

export const SubTitle = styled.Text`
  font-weight: bold;
`;
