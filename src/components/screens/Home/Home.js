import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { useFetchData } from '../../../hooks/useFetchData';
import { useRequestLocationPermission } from '../../../hooks/useRequestLocationPermission';
import { ActivityIndicator } from 'react-native';

import {
  Container,
  TitleLocale,
  Content,
  LeftCondition,
  Icon,
  LabelCondition,
  RightTemp,
  LabelTemp,
  SubTitle,
} from './styles';
import { LoadView } from '../Details/styles';
import { colors } from '../../../utils/colors';
import { getDataStored } from '../../../utils/localStore';

function Home() {
  useRequestLocationPermission();
  const loading = useFetchData();

  const forecastDay = useSelector(state => state.clima?.forecast);

  return (
    <Container>
      {!loading ? (
        <>
          <TitleLocale>{forecastDay.location?.name}</TitleLocale>
          <LabelCondition>
            {forecastDay.location?.region}/{forecastDay.location?.country}
          </LabelCondition>

          <Content>
            <LeftCondition>
              <Icon
                source={{
                  uri: `http:${forecastDay.current?.condition?.icon}`,
                }}
              />
              <LabelCondition>
                {forecastDay.current?.condition?.text}
              </LabelCondition>
            </LeftCondition>
            <RightTemp>
              <LabelTemp>{Math.round(forecastDay.current?.temp_c)}°</LabelTemp>
              <SubTitle>
                Sensação {Math.round(forecastDay.current?.feelslike_c)}°
              </SubTitle>
            </RightTemp>
          </Content>
        </>
      ) : (
        <LoadView>
          <ActivityIndicator size="large" color={colors.blackLigth} />
        </LoadView>
      )}
    </Container>
  );
}

export default Home;
