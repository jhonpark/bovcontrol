import styled from 'styled-components/native';
import { colors } from '../../../utils/colors';

export const Container = styled.View`
  flex: 1;
  padding: 5px 10px;
  flex-direction: row;
  background: ${colors.white};
  justify-content: space-between;
  border-bottom-width: 0.3px;
  border-color: ${colors.blackLigth};
  align-items: center;
`;

export const Left = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;

export const Icon = styled.Image`
  width: 40px;
  height: 40px;
`;
export const TextMax = styled.Text`
  font-weight: bold;
`;
export const TextMin = styled.Text`
  color: ${colors.blackLigth};
`;

export const Right = styled.View``;

export const Title = styled.Text`
  font-weight: bold;
`;
export const SubTitle = styled.Text`
  color: ${colors.blackLigth};
`;

export const ContentLeft = styled.View`
  padding: 10px;
`;
