import React, { useMemo } from 'react';
import { format } from 'date-fns';
import pt from 'date-fns/locale/pt';
import fromUnixTime from 'date-fns/fromUnixTime';

import {
  Container,
  Left,
  Icon,
  TextMax,
  TextMin,
  Right,
  Title,
  SubTitle,
  ContentLeft,
} from './styles';

export default function Weather(data) {
  const { date_epoch, day } = data.weather;

  const isoString = fromUnixTime(date_epoch);

  const date = useMemo(() => {
    return format(isoString, "dd 'de' MMMM 'de' yyyy", {
      locale: pt,
    });
  }, [date_epoch]);
  return (
    <Container>
      <Right>
        <Title>{date}</Title>
        <SubTitle>{day.condition.text}</SubTitle>
      </Right>
      <Left>
        <Icon source={{ uri: `http:${day.condition.icon}` }} />
        <ContentLeft>
          <TextMax>{Math.round(day.maxtemp_c)}°</TextMax>
          <TextMin>{Math.round(day.mintemp_c)}°</TextMin>
        </ContentLeft>
      </Left>
    </Container>
  );
}
