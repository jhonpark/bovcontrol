import { createStore, applyMiddleware, combineReducers } from 'redux';
import { reducer as network } from 'react-native-offline';
import thunk from 'redux-thunk';

import foreCastReducer from './forecastDay/reducer';

export const rootReducer = combineReducers({
  clima: foreCastReducer,
  network,
});

export default function configureStore(initialState = {}) {
  const middlewares = [thunk];
  const store = createStore(
    rootReducer,
    initialState,
    applyMiddleware(...middlewares),
  );
  return store;
}
