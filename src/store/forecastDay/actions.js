import { GET_ALL_FORECASTDAY } from './types';
import apiService from '../../services/apiServices';
import { storeData, getDataStored } from '../../utils/localStore';
import { Alert } from 'react-native';

export function fecthClimaForecast(latitude, longitude, isConnected) {
  return async dispatch => {
    if (isConnected) {
      apiService
        .getForecastClima(latitude, longitude)
        .then(list => {
          const forecast = list.data;
          storeData(forecast);
          dispatch({
            type: GET_ALL_FORECASTDAY,
            payload: { forecast },
          });
        })
        .catch(err => console.log(err));
    } else {
      getDataStored()
        .then(forecast => {
          dispatch({
            type: GET_ALL_FORECASTDAY,
            payload: { forecast },
          });
        })
        .catch(err => console.log(err));
    }
  };
}
