import { GET_ALL_FORECASTDAY } from './types';

const initialState = {
  forecast: {},
};

const forecastReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case GET_ALL_FORECASTDAY:
      return { ...state, ...payload };
    default:
      return state;
  }
};

export default forecastReducer;
