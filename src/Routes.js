import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';

import HomeScreen from './components/screens/Home';
import DetailsScreen from './components/screens/Details';

import { colors } from './utils/colors';

const Stack = createStackNavigator();
const Tab = createMaterialTopTabNavigator();

export default function Routes() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          options={{
            headerTitleAlign: 'center',
            headerTitle: 'Clima tempo',
            headerTintColor: '#fff',
            headerStyle: { backgroundColor: colors.primary },
          }}
          name="MyTabs"
          component={Tabs}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

function Tabs() {
  return (
    <Tab.Navigator>
      <Tab.Screen
        options={{ title: 'Hoje' }}
        name="Home"
        component={HomeScreen}
      />
      <Tab.Screen
        options={{ title: 'Semanal' }}
        name="Details"
        component={DetailsScreen}
      />
    </Tab.Navigator>
  );
}
