import AsyncStorage from '@react-native-community/async-storage';
const DATA_STORE = '@data_bovControl';

export function storeData(data) {
  AsyncStorage.setItem(DATA_STORE, JSON.stringify(data), () => {
    AsyncStorage.mergeItem(DATA_STORE, JSON.stringify(data), () => {});
  });
}

export async function getDataStored() {
  const user = await AsyncStorage.getItem(DATA_STORE);
  return JSON.parse(user);
}
