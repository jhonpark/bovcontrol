import React from 'react';
import Routes from './Routes';
import { StatusBar } from 'react-native';
import { Provider } from 'react-redux';
import configureStore from './store';
import { ReduxNetworkProvider } from 'react-native-offline';

import { colors } from './utils/colors';

const store = configureStore();

function App() {
  return (
    <Provider store={store}>
      <StatusBar barStyle="light-content" backgroundColor={colors.primary} />
      <ReduxNetworkProvider>
        <Routes />
      </ReduxNetworkProvider>
    </Provider>
  );
}

export default App;
