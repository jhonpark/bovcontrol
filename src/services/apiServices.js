import axios from 'axios';

const KEY = '13b3156ddcac48169fb21925201405';

const BASE_URL = 'http://api.weatherapi.com/v1/';
const GET_FORECAST = `forecast.json?key=${KEY}`;

const endPoints = {
  getClimeEndPoint: (lat, long) =>
    `${GET_FORECAST}&days=10&q=${lat},${long}&lang=pt`,
};

const bovControlApi = axios.create({
  baseURL: BASE_URL,
});

const apiService = {
  getForecastClima: (lat, long) =>
    bovControlApi.get(endPoints.getClimeEndPoint(lat, long)),
};

export default apiService;
