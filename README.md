## Supported platforms

- iOS
- Android

## Getting Started

### Install

No diretorio do projeto executa o comando

```
yarn install
```

### Link

- **React Native 0.60+**

[CLI autolink feature](https://github.com/react-native-community/cli/blob/master/docs/autolinking.md) links the module while building the app.

execute `pod install` no seu projeto ios:

```bash
$ cd ios && pod install && cd ..
```

Para executar no IOS e ANDROID

## Ios Simulador

```
yarn ios
```

## Android

```
yarn android
```

`
